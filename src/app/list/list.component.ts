import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { ListItemModel } from '../models/list-item.model';
import { YtdlService } from '../services/ytdl.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  listOfResult: ListItemModel[] = [];
  loading = false;

  constructor(private ytdlService: YtdlService) { }

  ngOnInit(): void {
    this.loading = true;
    this.ytdlService.getListOfDownloadedResources()
      .pipe(finalize(() => { this.loading = false; }))
      .subscribe(data => this.listOfResult = data);
  }

}
