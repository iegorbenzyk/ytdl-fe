import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { YtdlService } from '../services/ytdl.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {
  linkToVideo = '';
  isLoading = false;

  constructor(private ytdlService: YtdlService) { }

  ngOnInit(): void {
  }

  downloadVideo(): void {
    if (this.linkToVideo.length) {
      this.isLoading = true;
      this.ytdlService.downloadByUrl$(this.linkToVideo)
        .pipe(finalize(() => { this.isLoading = false; }))
        .subscribe(data => {
          this.linkToVideo = '';
          saveAs(data, 'video.mkv');
      });
    }
  }
}
