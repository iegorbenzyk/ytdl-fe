export class ListItemModel {
  // tslint:disable-next-line:variable-name
  constructor(public _id: string,
              public img: string,
              public title: string,
              public channel: string,
              public url: string,
              public createdAt: string,
              public updatedAt: string) {}
}
