import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ListItemModel } from '../models/list-item.model';

const baseUrl = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class YtdlService {

  constructor(private httpClient: HttpClient) {
  }

  downloadByUrl$(url: string): Observable<Blob> {
    return this.httpClient.post(`${baseUrl}/download`, { url }, {
      responseType: 'blob'
    });
  }

  getListOfDownloadedResources(): Observable<ListItemModel[]> {
    return this.httpClient.get<ListItemModel[]>(`${baseUrl}/list`);
  }
}
